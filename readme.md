# Laravel watcher
Syncing your code changes and trigger command

### How to use
- Setting script in `package.json`
```
"watch-theme": "laravel-watcher {source} {destination} {command}",
```
- Using
```
npm run watch-theme
```
