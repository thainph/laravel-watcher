#!/usr/bin/env node

const { exec } = require('child_process');
const chokidar = require('chokidar');

const sourcePath = process.argv[2] || null
const distPath = process.argv[3] || null
const command = process.argv[4] || null
const currentPath = process.cwd()

let deboundSync = null
let deboundTrigger = null
let isSynced = false
const ryncCommand = `rsync -avu --delete ${currentPath}/${sourcePath} ${currentPath}/${distPath}`
function watchChanges() {
    chokidar.watch(sourcePath, { ignored: /(^|[\/\\])\../ }).on('all', (event, path) => {
        syncFiles()
        triggerCommand(path)
    });
}
async function syncFiles() {
    if (deboundSync) {
        isSynced = false
        clearTimeout(deboundSync)
    }
    deboundSync = setTimeout(() => {
        exec(ryncCommand,  (error, stdout) => {
            console.log(stdout)
            isSynced = true
        })
    }, 500)
}
function triggerCommand(path) {
    if (deboundTrigger) {
        clearTimeout(deboundTrigger)
    }
    deboundTrigger = setTimeout(() => {
        let runCommand = command

        if (path && path.toString() !== '') {
            runCommand = `${command} --file=${currentPath}/${path}`
        }

        exec(runCommand, (error, stdout) => {
            console.log(stdout)
        })
    }, 3000)
}

watchChanges()
